ARG DOCKER_BASE_IMAGE_PREFIX
ARG DOCKER_BASE_IMAGE_NAMESPACE
ARG DOCKER_BASE_IMAGE_NAME=xamarin-ubuntu
ARG DOCKER_BASE_IMAGE_TAG=nuget-ubuntu
FROM ${DOCKER_BASE_IMAGE_PREFIX}${DOCKER_BASE_IMAGE_NAMESPACE}/${DOCKER_BASE_IMAGE_NAME}:${DOCKER_BASE_IMAGE_TAG}

ARG FIX_ALL_GOTCHAS_SCRIPT_LOCATION
ARG ETC_ENVIRONMENT_LOCATION
ARG CLEANUP_SCRIPT_LOCATION

# Depending on the base image used, we might lack wget/curl/etc to fetch ETC_ENVIRONMENT_LOCATION.
COPY environment.sh .
ADD $FIX_ALL_GOTCHAS_SCRIPT_LOCATION .
ADD $CLEANUP_SCRIPT_LOCATION .

# RUN wget --version
RUN set -o allexport \
    && . ./fix_all_gotchas.sh \
    && set +o allexport \
    && apt-get update \
    && apt-get install --assume-yes autoconf autotools-dev automake cmake build-essential curl gcc g++ g++-mingw-w64 gcc-mingw-w64 git libncurses5-dev libtool libz-mingw-w64-dev libzip-dev linux-libc-dev make ninja-build p7zip-full sqlite3 vim-common zlib1g-dev linux-libc-dev zlib1g-dev libtinfo6 \
    && git clone https://github.com/xamarin/xamarin-android.git \
    && cd xamarin-android \
    && cd .. \
    && . ./cleanup.sh
